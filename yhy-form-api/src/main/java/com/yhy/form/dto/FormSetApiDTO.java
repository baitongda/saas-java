package com.yhy.form.dto;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-8 上午11:03 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

import com.yhy.common.dto.BaseEntity;

public class FormSetApiDTO extends BaseEntity {
    private String formName;
    private String oaMenuName;
    private String formType;
    private String moduleBusClass;
    private String formViewUrl;
    private String formAddUrl;
    private String formEditUrl;
    private String formDeleteUrl;

    private String sysGenCode;

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getModuleBusClass() {
        return moduleBusClass;
    }

    public void setModuleBusClass(String moduleBusClass) {
        this.moduleBusClass = moduleBusClass;
    }

    public String getSysGenCode() {
        return sysGenCode;
    }

    public void setSysGenCode(String sysGenCode) {
        this.sysGenCode = sysGenCode;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getOaMenuName() {
        return oaMenuName;
    }

    public void setOaMenuName(String oaMenuName) {
        this.oaMenuName = oaMenuName;
    }

    public String getFormViewUrl() {
        return formViewUrl;
    }

    public void setFormViewUrl(String formViewUrl) {
        this.formViewUrl = formViewUrl;
    }

    public String getFormAddUrl() {
        return formAddUrl;
    }

    public void setFormAddUrl(String formAddUrl) {
        this.formAddUrl = formAddUrl;
    }

    public String getFormEditUrl() {
        return formEditUrl;
    }

    public void setFormEditUrl(String formEditUrl) {
        this.formEditUrl = formEditUrl;
    }

    public String getFormDeleteUrl() {
        return formDeleteUrl;
    }

    public void setFormDeleteUrl(String formDeleteUrl) {
        this.formDeleteUrl = formDeleteUrl;
    }
}
