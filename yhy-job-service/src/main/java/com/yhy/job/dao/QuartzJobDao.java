package com.yhy.job.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import com.yhy.job.vo.QuartzJobVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午2:42 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "quartzJobDao")
public interface QuartzJobDao extends BaseMainDao<QuartzJobVO> {

    void updateRunStatus(QuartzJobVO entity);

}
