package com.yhy.job.dao;

import com.yhy.common.dao.BaseMngDao;
import com.yhy.job.dto.QuartzJobDTO;
import com.yhy.job.vo.mng.QuartzJobMngVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午2:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "quartzJobMngDao")
public interface QuartzJobMngDao extends BaseMngDao<QuartzJobDTO,QuartzJobMngVO> {

    List<QuartzJobMngVO> findAllValidJob();

}
