package com.yhy.admin.dao;

import com.yhy.admin.dto.MenuDTO;
import com.yhy.admin.vo.mng.MenuMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-1 下午4:58 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Mapper
@Component(value = "menuMngDao")
public interface MenuMngDao extends BaseMngDao<MenuDTO,MenuMngVO> {

    List<MenuMngVO> findAllMenu(@Param("lanCode")String lanCode);

    List<MenuMngVO> findAllValidRecord(@Param("lanCode")String lanCode, @Param("isAdmin")String isAdmin);

    List<MenuMngVO> findCommonAdminMenu(@Param("lanCode")String lanCode);

    List<MenuMngVO> findMenuByRoleids(@Param("roleids")Set<String> roleids, @Param("isAdmin")String isAdmin, @Param("lanCode")String lanCode);

    List<MenuMngVO> findMenuPrivByRoleids(@Param("roleids")Set<String> roleids);

}
