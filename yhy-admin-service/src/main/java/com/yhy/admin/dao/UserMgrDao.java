package com.yhy.admin.dao;

import com.yhy.admin.vo.UserMgrVO;
import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-26 下午5:39 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "userMgrDao")
public interface UserMgrDao extends BaseDao<UserMgrVO> {

    int deleteByUserAccount(@Param("cpyCode")String cpyCode, @Param("userAccount") String userAccount);

    UserMgrVO findMgrByUserAccount(@Param("cpyCode")String cpyCode, @Param("userAccount")String userAccount);

}
