package com.yhy.admin.dao;

import com.yhy.admin.vo.SysUserJobVO;
import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:44 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysUserJobDao")
public interface SysUserJobDao  extends BaseDao<SysUserJobVO> {

    int deleteByUserId(@Param("cpyCode")String cpyCode, @Param("userId") String userId);

    SysUserJobVO findJobByUserId(@Param("cpyCode")String cpyCode, @Param("userId")String userId);

}
