package com.yhy.admin.dto;

import com.yhy.admin.vo.MenuLanVO;
import com.yhy.admin.vo.MenuVO;
import com.yhy.admin.vo.mng.MenuMngVO;
import com.yhy.common.dto.BaseMngDTO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:02 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class MenuDTO extends BaseMngDTO<MenuMngVO> {

    private List<MenuLanVO> menuLanVOList;

    @Override
    public MenuVO getBusMain() {
        return new MenuVO();
    }

    public List<MenuLanVO> getMenuLanVOList() {
        return menuLanVOList;
    }

    public void setMenuLanVOList(List<MenuLanVO> menuLanVOList) {
        this.menuLanVOList = menuLanVOList;
    }


}
