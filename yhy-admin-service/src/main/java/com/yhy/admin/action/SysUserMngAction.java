package com.yhy.admin.action;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.yhy.admin.dto.LoginDTO;
import com.yhy.admin.dto.OrgSetDTO;
import com.yhy.admin.dto.SysCustomFieldDTO;
import com.yhy.admin.dto.SysUserDTO;
import com.yhy.admin.service.LoginService;
import com.yhy.admin.service.SysCustomFieldService;
import com.yhy.admin.service.UserCpyService;
import com.yhy.admin.service.mng.*;
import com.yhy.admin.vo.SysCustomFieldVO;
import com.yhy.admin.vo.UserCpyVO;
import com.yhy.admin.vo.mng.MenuMngVO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.constants.BusMenuType;
import com.yhy.common.constants.BusPrivCode;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.intercept.LoginPermission;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.service.SysAttachmentService;
import com.yhy.common.utils.ConstantUtil;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.RedisUtil;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysAttachment;
import com.yhy.common.vo.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:37 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/userMng", produces="application/json;charset=UTF-8")
public class SysUserMngAction extends BaseMngAction<SysUserDTO> {

    @Autowired
    private SysUserMngService sysUserMngService;

    @Autowired
    private SysAttachmentService sysAttachmentService;

    @Autowired
    private LoginService loginService;
    @Autowired
    private UserCpyService userCpyService;
    @Autowired
    private MenuMngService menuMngService;
    @Autowired
    private SysRoleMngService sysRoleMngService;
    @Autowired
    private OrgSetMngService orgSetMngService;
    @Autowired
    private SysCustomFieldService sysCustomFieldService;
    @Autowired
    private FtpManageService ftpManageService;
    @Override
    protected SysUserMngService getBaseService() {
        return sysUserMngService;
    }


    @LoginPermission(loginRequired = false,name = "用户注册")
    @RequestMapping(value="/userRegister",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="用户注册", notes="用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysUserDTO")
    })
    public AppReturnMsg userRegister(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        SysUserMngVO userMngVO = baseDTO.getBusMainData();
        //2.校验用户是否存在
        SysUser sysUser = getBaseService().findByUserAccount(userMngVO.getUserAccount());
        if(sysUser != null) {
            throw new BusinessException("该用户已存在.");
        }
        //1.校验密码
        if(!userMngVO.getPassword().equals(userMngVO.getConfirmPassword())) {
            throw new BusinessException("密码不一致.");
        }
        if(StringUtils.isAnyBlank(userMngVO.getUserName(),userMngVO.getPhone())) {
            throw new BusinessException("姓名/手机等信息不能为空.");
        }
        sysUser = getBaseService().findByPhone(userMngVO.getPhone());
        if(sysUser != null) {
            throw new BusinessException("该手机号已被注册.");
        }
        userMngVO.setCreateBy(userMngVO.getUserAccount());
        userMngVO.setLastUpdateBy(userMngVO.getUserAccount());
        userMngVO.setUserStatus("UNLOCK");
        AppReturnMsg rtnmsg =  super.doSaveCommon(request, response, baseDTO);
        rtnmsg.setMsg(null);
        return rtnmsg;
    }

    /*
     * 登录认证
     */
    @RequestMapping(value="/login",method = {RequestMethod.POST})
    @ResponseBody
    @LoginPermission(loginRequired = false,name = "登录认证")
    @ApiOperation(value="登录认证", notes="根据用户及密码校验登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginDTO", value = "登录实体", required = true, dataType = "LoginDTO")
    })
    AppReturnMsg login(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginDTO loginDTO) {
        //根据传入参数校验登录
        loginDTO.setUserAccount(StringUtils.lowerCase(loginDTO.getUserAccount()));
        SysUser sysUser = loginService.checkUserAndPassword(loginDTO);
        if(sysUser == null) {
            return new AppReturnMsg(ReturnCode.LOGIN_FAIL.getCode(),"登录失败",null,null);
        }
        sysUser.setLanCode(loginDTO.getLanCode());
        String token = loginService.tokenOfLoginSucess(loginDTO,sysUser);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"登录成功",sysUser,token);
    }

    /*
     * 获取登录人信息
     */
    @RequestMapping(value="/info",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取用户信息", notes="获取用户信息")
    AppReturnMsg info(HttpServletRequest request, HttpServletResponse response) {
        //根据传入参数校验登录
        SysUser sysUser = YhyUtils.getSysUser();
        SysUser newSysUser = sysUserMngService.findByUserAccount(sysUser.getUserAccount());
        sysUser.setAvatar(newSysUser.getAvatar());
        sysUser.setCpyCode(newSysUser.getCpyCode());
        sysUser.setCpyName(newSysUser.getCpyName());
        sysUser.setOrgName(newSysUser.getOrgName());
        sysUser.setOrgCode(newSysUser.getOrgCode());
        sysUser.setEmail(newSysUser.getEmail());
        sysUser.setWeixin(newSysUser.getWeixin());
        sysUser.setUserName(newSysUser.getUserName());
        sysUser.setPhone(newSysUser.getPhone());

        if (sysUser.isSysAdmin()) {
            sysUser.setRoles(Sets.newHashSet(BusPrivCode.PRIV_SYS_ADMIN.getVal()));
            if(StringUtils.isBlank(sysUser.getCpyCode())) {
                sysUser.setCpyCode(BusPrivCode.PRIV_SYS_ADMIN.getVal());
            }
            if(StringUtils.isBlank(sysUser.getOrgCode())) {
                sysUser.setOrgCode(BusPrivCode.PRIV_SYS_ADMIN.getVal());
            }
            sysUser.setRoles(Sets.newHashSet(BusPrivCode.PRIV_SYS_ADMIN.getVal()));
            sysUser.setPrivOrgCodes(Sets.newHashSet(BusPrivCode.PRIV_SYS_ADMIN.getVal()));
            sysUser.setUserOrgCodes(Sets.newHashSet(BusPrivCode.PRIV_SYS_ADMIN.getVal()));
            RedisUtil.set(ConstantUtil.PRIV_CODE_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount(),Sets.newHashSet(BusPrivCode.PRIV_SYS_ADMIN.getVal()));
            RedisUtil.set(ConstantUtil.OPERATOR_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount(),sysUser);
            return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),sysUser);
        }

        //获取用户所属组织
        Set<String> userOrgCodeCaches = sysUser.getUserOrgCodes();
        if(userOrgCodeCaches == null) {
            Set<String> tmpCodeCaches = Sets.newHashSet();
            List<OrgSetMngVO> orgSetMngVOList = orgSetMngService.findOrgSetByUserCpyCode(sysUser.getUserAccount(),sysUser.getCpyCode());
            orgSetMngVOList.stream().forEach(source ->{
                tmpCodeCaches.add(source.getOrgCode());
            });
            //没有设置所属组织
            if(tmpCodeCaches.isEmpty()) {
                if(StringUtils.isNotBlank(sysUser.getOrgCode())) {
                    tmpCodeCaches.add(sysUser.getOrgCode());
                } else {
                    tmpCodeCaches.add("noorgcode");
                }
            }
            sysUser.setUserOrgCodes(tmpCodeCaches);
        } else {
            sysUser.setUserOrgCodes(userOrgCodeCaches);
        }

        Set<String> roleids = sysRoleMngService.findRoleByUserInfo(sysUser.getCpyCode(),sysUser.getUserAccount());
        if (roleids.isEmpty()) {
            RedisUtil.set(ConstantUtil.OPERATOR_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount(),sysUser);
            return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),sysUser);
        }

        //获取权限代码
        Object cacheCodes = RedisUtil.get(ConstantUtil.PRIV_CODE_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount());
        if (cacheCodes == null) {
            Set<String> privCodes = Sets.newHashSet();
            List<MenuMngVO> privLeafMenu = menuMngService.findMenuPrivByRoleids(roleids);
            privLeafMenu.stream().forEach(source ->{
                if(BusMenuType.MENU_PRIV.getVal().equals(source.getMenuType())
                    && BusPrivCode.PRIV_COMMON_ADMIN.getVal().equals(source.getPrivCode())) {
                    privCodes.add(source.getPrivCode());
                    return;
                }
            });
            //没有设置所属权限,
            if(privCodes.isEmpty()) {
                privCodes.add("norolecode");
            }
            sysUser.setRoles(privCodes);
            RedisUtil.set(ConstantUtil.PRIV_CODE_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount(),privCodes);
        } else {
            sysUser.setRoles((Set<String>) cacheCodes);
        }

        //获取权限组织
        Set<String> orgCodeCaches = sysUser.getPrivOrgCodes();
        if(orgCodeCaches == null) {
            Set<String> tmpCodeCaches = Sets.newHashSet();
            List<OrgSetMngVO> privOrgSetList = sysRoleMngService.findOrgSetByRoleids(roleids,null);
            privOrgSetList.stream().forEach(source ->{
                tmpCodeCaches.add(source.getOrgCode());
            });
            //没有设置所属权限,
            if(tmpCodeCaches.isEmpty()) {
                if(StringUtils.isNotBlank(sysUser.getOrgCode())) {
                    tmpCodeCaches.add(sysUser.getOrgCode());
                } else {
                    tmpCodeCaches.add("noorgcode");
                }
            }
            sysUser.setPrivOrgCodes(tmpCodeCaches);
        } else {
            sysUser.setPrivOrgCodes(orgCodeCaches);
        }
        RedisUtil.set(ConstantUtil.OPERATOR_PREFIX + sysUser.getSourceType() + sysUser.getUserAccount(),sysUser);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),sysUser);
    }

    /*
     * 更改头像
     */
    @RequestMapping(value="/updateAvatar",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="更改头像", notes="更改头像")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "文件域", required = true, dataType = "MultipartFile")
    })
    AppReturnMsg updateAvatar(HttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile file) {
        SysUser sysUser = YhyUtils.getSysUser();
        SysAttachment sysAttachment = new SysAttachment();
        sysAttachment.setBusModule(getBaseService().getBusModule());
        sysAttachment.setMainId(sysUser.getId());
        sysAttachment.setCreateBy(sysUser.getUserAccount());
        ftpManageService.upload(sysAttachment, file, true);
        sysAttachmentService.insert(sysAttachment);
        //sysUserMngService.updateAvatar(sysUser.getUserAccount(),sysAttachment.getFileUrl());
        sysUserMngService.updateAvatar(sysUser.getUserAccount(),sysAttachment.getFilePath());

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"更改头像成功",null,null);
    }

    /*
     * 注销
     */
    @RequestMapping(value="/logout",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="注销登录", notes="根据用户注销登录")
    @ApiImplicitParams({
            //@ApiImplicitParam(name = "id", value = "图书ID", required = true, dataType = "Long",paramType = "path"),
            @ApiImplicitParam(name = "loginDTO", value = "注销实体", required = true, dataType = "LoginDTO")
    })
    @LoginPermission(loginRequired = true)
    AppReturnMsg logout(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginDTO loginDTO) {
        //根据传入参数校验登录
        SysUser sysUser = YhyUtils.getSysUser();
        loginDTO = new LoginDTO();
        loginDTO.setUserAccount(sysUser.getUserAccount());
        /*if(StringUtils.isEmpty(loginDTO.getUserAccount())) {
            throw new BusinessException("用户不能为空");
        }*/
        loginService.logout(loginDTO);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"注销成功",null,null);
    }

    @RequestMapping(value="/updatePassword",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="更改密码", notes="密码变更")
    @ApiImplicitParams({
            //@ApiImplicitParam(name = "id", value = "图书ID", required = true, dataType = "Long",paramType = "path"),
            @ApiImplicitParam(name = "loginDTO", value = "密码变更实体", required = true, dataType = "LoginDTO")
    })
    AppReturnMsg updatePassword(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginDTO loginDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        loginDTO.setUserAccount(sysUser.getUserAccount());
        loginService.changePassword(loginDTO);

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"密码变更成功",null,null);
    }

    @RequestMapping(value="/updateCpyOrgSet",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="更改默认公司及权限部门", notes="更改默认公司及权限部门")
    @ApiImplicitParams({
            //@ApiImplicitParam(name = "id", value = "图书ID", required = true, dataType = "Long",paramType = "path"),
            @ApiImplicitParam(name = "loginDTO", value = "密码变更实体", required = true, dataType = "LoginDTO")
    })
    AppReturnMsg updateCpyOrgSet(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUser paramDTO) {
        SysUser sysUser = YhyUtils.getSysUser();

        sysUserMngService.updateCpyOrgSet(sysUser.getUserAccount(), paramDTO.getCpyCode(),paramDTO.getOrgCode());

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"变更成功",null,null);
    }

    /*
     * 根据用户获取所属公司
     */
    @RequestMapping(value="/getCpyByUserAccount",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="根据用户获取所属公司", notes="根据用户获取所属公司")
    AppReturnMsg getCpyByUserAccount(HttpServletRequest request, HttpServletResponse response) {
        //根据传入参数校验登录
        SysUser sysUser = YhyUtils.getSysUser();
        List<UserCpyVO> userCpyVOList = userCpyService.findUserCpyByUserAccount(sysUser.getUserAccount());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",userCpyVOList,null);
    }

    /*
     * 根据用户获取有权限的组织
     */
    @RequestMapping(value="/getPrivOrgSetByUserAccount",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="根据用户获取有权限的组织", notes="根据用户获取有权限的组织")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息", required = true, dataType = "SysUser")
    })
    AppReturnMsg getPrivOrgSetByUserAccount(HttpServletRequest request, HttpServletResponse response,
                                     @RequestBody SysUser paramDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        //根据用户获取有权限的组织
        Set<String> roleids = sysRoleMngService.findRoleByUserInfo(paramDTO.getCpyCode(),sysUser.getUserAccount());
        if(roleids.isEmpty()) {
            return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),null,Lists.newArrayList(),null);
        }

        //获取该公司所有组织
        OrgSetDTO baseDTO =new OrgSetDTO();
        OrgSetMngVO mngVO = new OrgSetMngVO();
        mngVO.setCpyCode(paramDTO.getCpyCode());
        mngVO.setEnableFlag("Y");
        baseDTO.setParamBean(mngVO);
        List<OrgSetMngVO> allOrgSetList = orgSetMngService.queryPageData(baseDTO);
        Map<String,OrgSetMngVO> allMap = Maps.newHashMap();
        for (OrgSetMngVO tmpMngVO : allOrgSetList) {
            allMap.put(tmpMngVO.getId(),tmpMngVO);
        }

        List<OrgSetMngVO> privSetMngList = sysRoleMngService.findOrgSetByRoleids(roleids,null);
        Set<String> privSets = Sets.newHashSet();
        List<OrgSetMngVO> resultPrivList = Lists.newArrayList(privSetMngList);
        privSetMngList.stream().forEach(orgSetMngVO -> { privSets.add(orgSetMngVO.getId()); });

        for (OrgSetMngVO tmpMngVo:privSetMngList) {
            if(StringUtils.isBlank(tmpMngVo.getParentId())) {
                continue;
            }
            if(!privSets.contains(tmpMngVo.getParentId())) {
                //没有父对象权限
                OrgSetMngVO tmpVO = allMap.get(tmpMngVo.getParentId());
                tmpVO.setDisabled(true);
                resultPrivList.add(tmpVO);
            }
        }
        Set<OrgSetMngVO> tree = orgSetMngService.buildOrgTree(resultPrivList);

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", JsonUtils.tranList(tree, HashMap.class),null);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSUSER_ALL","SYSUSER_CREATE","SYSUSER_CHANGE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysUserDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSUSER_ALL","SYSUSER_CREATE","SYSUSER_CHANGE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysUserDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSUSER_ALL","SYSUSER_CREATE","SYSUSER_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysUserDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }


    @RequestMapping(value="/doSaveUserInfo",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存用户信息", notes="保存用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUserMngVO", value = "用户实体", required = true, dataTypeClass = SysUserMngVO.class)
    })
    public AppReturnMsg doSaveUserInfo(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserMngVO sysUserMngVO) {
        sysUserMngVO.setCpyCode(YhyUtils.getSysUser().getCpyCode());
        getBaseService().updateUserInfo(sysUserMngVO);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",sysUserMngVO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSUSER_ALL","SYSUSER_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysUserDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        sysUser.setCpyCode(null);
        baseDTO.setSysUser(sysUser);
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSUSER_ALL","SYSUSER_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "SysUserDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/findUserByOrgCode",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据公司及组织获取用户信息", notes="根据公司及组织获取用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织", required = false, dataType = "String")

    })
    public AppReturnMsg findUserByOrgCode(HttpServletRequest request, HttpServletResponse response,
                                          @RequestBody SysUserDTO userDTO) {
        List<SysUserMngVO> sysUsers = getBaseService().findUserByOrgCode(userDTO.getParamBean().getCpyCode(),userDTO.getParamBean().getOrgCode());
        userDTO.setRtnList(sysUsers);
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",userDTO);
        return returnMsg;
    }

    @RequestMapping(value="/findUserByCpyCode",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据公司获取所有用户信息", notes="根据公司获取所有用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司", required = true, dataTypeClass = SysUserDTO.class),

    })
    public AppReturnMsg findUserByCpyCode(HttpServletRequest request, HttpServletResponse response,
                                          @RequestBody SysUserDTO userDTO) {
        List<SysUserMngVO> sysUsers = getBaseService().findUserByCpyCode(userDTO.getParamBean().getCpyCode());
        userDTO.setRtnList(sysUsers);
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",userDTO);
        return returnMsg;
    }

    @RequestMapping(value="/deleteByGridIdAndUser",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="根据表格ID恢复系统默认显示", notes="根据表格ID恢复系统默认显示")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gridId", value = "表格ID", required = true, dataTypeClass = String.class),

    })
    public AppReturnMsg deleteByGridIdAndUser(HttpServletRequest request, HttpServletResponse response,
                                          @RequestParam(name = "gridId") String gridId) {
        SysUser sysUser = YhyUtils.getSysUser();
        sysCustomFieldService.deleteByGridIdAndUser(gridId, sysUser.getUserAccount(), sysUser.getCpyCode());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"已恢复系统默认", null);
    }

    @RequestMapping(value="/saveCustomField",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存用户自定义显示", notes="保存用户自定义显示")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "保存对象", value = "", required = true, dataTypeClass = SysCustomFieldDTO.class),

    })
    public AppReturnMsg saveCustomField(HttpServletRequest request, HttpServletResponse response,
                                        @RequestBody SysCustomFieldDTO customFieldDTO) {
        customFieldDTO.setSysUser(YhyUtils.getSysUser());
        sysCustomFieldService.saveCustomField(customFieldDTO);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功", null);
    }

    @RequestMapping(value="/findByGridIdAndUser",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="根据表格ID获取用户定义信息", notes="根据表格ID获取用户定义信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gridId", value = "表格ID", required = true, dataTypeClass = String.class),

    })
    public AppReturnMsg findByGridIdAndUser(HttpServletRequest request, HttpServletResponse response,
                                              @RequestParam(name = "gridId") String gridId) {
        SysUser sysUser = YhyUtils.getSysUser();
        List<SysCustomFieldVO> customFieldVOS = sysCustomFieldService.findByGridIdAndUser(gridId, sysUser.getUserAccount(), sysUser.getCpyCode());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", customFieldVOS);
    }

}
