package com.yhy.admin.action;

import com.yhy.admin.dto.SysPrintDTO;
import com.yhy.admin.service.mng.SysPrintMngService;
import com.yhy.admin.vo.mng.SysPrintMngVO;
import com.yhy.common.action.BaseComplexMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.YhyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午5:23 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/sysPrintMng", produces="application/json;charset=UTF-8")
public class SysPrintMngAction extends BaseComplexMngAction<SysPrintDTO> {

    @Autowired
    private SysPrintMngService printMngService;

    @Override
    protected SysPrintMngService getBaseService() {
        return printMngService;
    }

    /*
     * 获取报表定义
     */
    @RequestMapping(value="/getPrintByReportCode",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取报表定义", notes="获取报表定义")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "reportCode", value = "报表编码", required = true, dataType = "String")
    })
    public AppReturnMsg getPrintByReportCode(@RequestParam(value = "reportCode",required = true)String reportCode,HttpServletRequest request, HttpServletResponse response) {
        List<SysPrintMngVO> printMainVOS = printMngService.findValidDataByReportCode(reportCode, YhyUtils.getSysUser().getCpyCode(),YhyUtils.getSysUser().getLanCode());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",printMainVOS,null);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_CREATE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysPrintDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_CREATE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysPrintDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_CREATE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_CHANGE"})
    @RequestMapping(value="/toChange",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="变更操作", notes="变更操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysPrintDTO.class)
    })
    public AppReturnMsg toChange(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.toChangeCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_SUBMIT"})
    @RequestMapping(value="/doSubmit",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="提交", notes="提交操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysPrintDTO.class)
    })
    public AppReturnMsg doSubmit(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.doSubmitCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSPRINT_ALL","SYSPRINT_DESTORY"})
    @RequestMapping(value="/doDestory",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="注销操作", notes="注销操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysPrintDTO.class)
    })
    public AppReturnMsg doDestory(HttpServletRequest request, HttpServletResponse response, @RequestBody SysPrintDTO baseDTO) {
        return super.doDestoryCommon(request, response, baseDTO);
    }

}
