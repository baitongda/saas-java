package com.yhy.form.dao;

import com.yhy.common.dao.BaseMainDao;
import com.yhy.form.vo.FormApplyMainVO;

import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formApplyMainDao")
public interface FormApplyMainDao  extends BaseMainDao<FormApplyMainVO> {

}
