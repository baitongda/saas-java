package com.yhy.form.vo;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午10:43 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public class FormSetFieldVO extends BaseEntity {

    private String mainId;
    private String fieldType;
    private String fieldControlId;
    private String fieldName;
    private String fieldDefaultValue;
    private String fieldList;
    private String fieldAttr1;
    private String fieldAttr2;
    private String privFlag;
    private String requireFlag;
    private String cssAttribute;
    private String jsAttribute;


    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldControlId() {
        return fieldControlId;
    }

    public void setFieldControlId(String fieldControlId) {
        this.fieldControlId = fieldControlId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldDefaultValue() {
        return fieldDefaultValue;
    }

    public void setFieldDefaultValue(String fieldDefaultValue) {
        this.fieldDefaultValue = fieldDefaultValue;
    }

    public String getFieldList() {
        return fieldList;
    }

    public void setFieldList(String fieldList) {
        this.fieldList = fieldList;
    }

    public String getFieldAttr1() {
        return fieldAttr1;
    }

    public void setFieldAttr1(String fieldAttr1) {
        this.fieldAttr1 = fieldAttr1;
    }

    public String getFieldAttr2() {
        return fieldAttr2;
    }

    public void setFieldAttr2(String fieldAttr2) {
        this.fieldAttr2 = fieldAttr2;
    }

    public String getPrivFlag() {
        return privFlag;
    }

    public void setPrivFlag(String privFlag) {
        this.privFlag = privFlag;
    }

    public String getRequireFlag() {
        return requireFlag;
    }

    public void setRequireFlag(String requireFlag) {
        this.requireFlag = requireFlag;
    }

    public String getCssAttribute() {
        return cssAttribute;
    }

    public void setCssAttribute(String cssAttribute) {
        this.cssAttribute = cssAttribute;
    }

    public String getJsAttribute() {
        return jsAttribute;
    }

    public void setJsAttribute(String jsAttribute) {
        this.jsAttribute = jsAttribute;
    }
}
