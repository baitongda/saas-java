package com.yhy.form.api.impl;

import com.google.common.collect.Lists;
import com.yhy.common.constants.BusClassType;
import com.yhy.common.constants.BusSelfState;
import com.yhy.common.dto.BaseMainEntity;
import com.yhy.common.dto.BaseMngVO;
import com.yhy.common.dto.workflow.WorkflowInfo;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.service.BusCommonStateService;
import com.yhy.common.utils.BusCenterHelper;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.form.api.IFormService;
import com.yhy.form.dto.FormSetApiDTO;
import com.yhy.form.service.mng.FormSetMngService;
import com.yhy.form.vo.mng.FormSetMngVO;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-8 上午11:05 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Transactional
@Service(IFormService.SERVICE_BEAN)
public class FormServiceApi implements IFormService {


    @Autowired
    private FormSetMngService formSetMngService;

    @Override
    public List<FormSetApiDTO> getFormSetByCode(Set<String> codes) {
        Validate.notNull(codes);
        if(codes.isEmpty()) {
            return Lists.newArrayList();
        }
        List<FormSetMngVO> formSetMngVOList = formSetMngService.getFormSetByCode(codes);
        return JsonUtils.tranList(formSetMngVOList, FormSetApiDTO.class);
    }

    @Override
    public Boolean updateWorkflowSelfState(String userAccount, BusSelfState busSelfState, WorkflowInfo workflowInfo) {
        BaseMngService baseMngService = SpringContextHolder.getBean(BusClassType.getInstByVal(workflowInfo.getBusClass()).getMngService());
        baseMngService.workflowCallbackHandle(userAccount, busSelfState, workflowInfo);
        return true;
    }

}
