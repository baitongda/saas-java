package com.yhy.form.dto;

import com.google.common.collect.Lists;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.form.vo.FormApplyFieldValueVO;
import com.yhy.form.vo.FormApplyMainVO;
import com.yhy.form.vo.FormSetContentVO;
import com.yhy.form.vo.FormSetMainVO;
import com.yhy.form.vo.mng.FormApplyMngVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午11:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormApplyDTO extends BaseMngDTO<FormApplyMngVO> {

    // 表单信息内容
    private FormSetContentVO formContentInfo;
    private FormSetMainVO formInfo;

    //字段值对象 key->value
    private Map<String,Object> formData;
    private Map<String,Boolean> formDataVisible;
    private Map<String,Boolean> formDataEditable;

    @Override
    public FormApplyMainVO getBusMain() {
        return new FormApplyMainVO();
    }

    public FormSetMainVO getFormInfo() {
        return formInfo;
    }

    public void setFormInfo(FormSetMainVO formInfo) {
        this.formInfo = formInfo;
    }

    public FormSetContentVO getFormContentInfo() {
        return formContentInfo;
    }

    public void setFormContentInfo(FormSetContentVO formContentInfo) {
        this.formContentInfo = formContentInfo;
    }

    public Map<String, Object> getFormData() {
        return formData;
    }

    public void setFormData(Map<String, Object> formData) {
        this.formData = formData;
    }

    public Map<String, Boolean> getFormDataVisible() {
        return formDataVisible;
    }

    public void setFormDataVisible(Map<String, Boolean> formDataVisible) {
        this.formDataVisible = formDataVisible;
    }

    public Map<String, Boolean> getFormDataEditable() {
        return formDataEditable;
    }

    public void setFormDataEditable(Map<String, Boolean> formDataEditable) {
        this.formDataEditable = formDataEditable;
    }
}
