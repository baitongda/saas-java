package com.yhy.common.utils;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-9-2 下午3:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

import cn.hutool.core.io.FileUtil;
import com.google.common.io.Files;
import com.yhy.common.config.EnvConfig;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * <br>
 * <b>功能：</b><br>
 * <b>作者：</b>yanghuiyaun<br>
 * <b>日期：</b> 2019/9/2 <br>
 * <b>版权所有：<b>版权所有(C) 2019<br>
 */
public class FileUtils extends FileUtil {

    /**
     * 定义GB的计算常量
     */
    private static final int GB = 1024 * 1024 * 1024;
    /**
     * 定义MB的计算常量
     */
    private static final int MB = 1024 * 1024;
    /**
     * 定义KB的计算常量
     */
    private static final int KB = 1024;

    /**
     * 格式化小数
     */
    private static final DecimalFormat DF = new DecimalFormat("0.00");

    /**
     * 根据文件名 获取临时文件完整路径
     * @param fileName
     * @return
     */
    public static String getTempFullPathByName(String fileName) {
        // 获取文件后缀
        String uploadTempDir = EnvConfig.getValueByPropertyName("file.tmp.dir");
        File uploadTempDirPath = new File(uploadTempDir);
        if (!uploadTempDirPath.exists()) {
            uploadTempDirPath.mkdirs();
        }
        // 用uuid作为文件名，防止生成的临时文件重复
        return uploadTempDir + "/" + fileName;
    }

    public static File toFile(InputStream inputStream, String filePath) {
        try {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            File targetFile = new File(filePath);
            Files.write(buffer, targetFile);
            return targetFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * MultipartFile转File
     * @param multipartFile
     * @return
     */
    public static File toFile(MultipartFile multipartFile){
        // 获取文件名
        String fileName = multipartFile.getOriginalFilename();
        // 获取文件后缀
        String suffix ="."+getExtensionName(fileName);
        File file = null;
        String tempfileName = YhyUtils.generateUUID() + suffix;
        try {
            String uploadTempDir = EnvConfig.getValueByPropertyName("file.tmp.dir");
            File uploadTempDirPath = new File(uploadTempDir);
            if (!uploadTempDirPath.exists()) {
                uploadTempDirPath.mkdirs();
            }
            // 用uuid作为文件名，防止生成的临时文件重复
            file = new File(uploadTempDir + "/" + tempfileName);
            //file = File.createTempFile(YhyUtils.generateUUID(), prefix);
            // MultipartFile to File
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * 删除
     * @param files
     */
    public static void deleteFile(File... files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 获取文件扩展名
     * @param filename
     * @return
     */
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }

    /**
     * Java文件操作 获取不带扩展名的文件名
     * @param filename
     * @return
     */
    public static String getFileNameNoEx(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length()))) {
                return filename.substring(0, dot);
            }
        }
        return filename;
    }

    /**
     * 文件大小转换
     * @param size
     * @return
     */
    public static String getSize(int size){
        String resultSize = "";
        if (size / GB >= 1) {
            //如果当前Byte的值大于等于1GB
            resultSize = DF.format(size / (float) GB) + "GB   ";
        } else if (size / MB >= 1) {
            //如果当前Byte的值大于等于1MB
            resultSize = DF.format(size / (float) MB) + "MB   ";
        } else if (size / KB >= 1) {
            //如果当前Byte的值大于等于1KB
            resultSize = DF.format(size / (float) KB) + "KB   ";
        } else {
            resultSize = size + "B   ";
        }
        return resultSize;
    }
}
