package com.yhy.common.dao;

import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-20 下午7:21 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public interface BaseMainDao<T extends BaseMainEntity> extends BaseDao<T> {

    //用于主表更新 version
    Integer updateVersion(T entity);

    Integer updateBusState(T entity);

    Integer updateEnableFlagForRecover(@Param("id") String id,@Param("enableFlag") String enableFlag);

}
