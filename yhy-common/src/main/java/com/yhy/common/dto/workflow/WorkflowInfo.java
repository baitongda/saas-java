package com.yhy.common.dto.workflow;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-5-12 上午11:34 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class WorkflowInfo extends BaseEntity {

    //任务名称
    private String taskTitle;
    private String taskDesc;

    private String businessKey;
    private String busClass;
    private String busModule;
    private String formCode;

    private String formUrl;
    //活动节点
    private String activityId;
    private String activityName;
    //流程实例ID
    private String processInstanceId;

    //直接跳过审批
    private Boolean passByDirect = false;

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    public Boolean getPassByDirect() {
        return passByDirect;
    }

    public void setPassByDirect(Boolean passByDirect) {
        this.passByDirect = passByDirect;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getBusClass() {
        return busClass;
    }

    public void setBusClass(String busClass) {
        this.busClass = busClass;
    }

    public String getBusModule() {
        return busModule;
    }

    public void setBusModule(String busModule) {
        this.busModule = busModule;
    }

}
