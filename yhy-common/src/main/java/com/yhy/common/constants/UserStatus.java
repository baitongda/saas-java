package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:02 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public enum UserStatus {

	LOCK("LOCK","锁定"),

	UNLOCK("UNLOCK","解锁");

	private final String val;
	private final String labCode;

	private static Map<String, UserStatus> userStatusMap;

	UserStatus(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static UserStatus getInstByVal(String key) {
		if(userStatusMap == null) {
			synchronized (UserStatus.class) {
				if (userStatusMap == null) {
					userStatusMap = new HashMap<String, UserStatus>();
					for (UserStatus busState : UserStatus.values()) {
						userStatusMap.put(busState.getVal(), busState);
					}
				}
			}
		}

		if (!userStatusMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的userStatusMap枚举值不存在。");
		}
		return userStatusMap.get(key);
	}

}
