<h1 style="text-align: center">简云Saas平台</h1>
<div style="text-align: center">

[![Git Star] (https://gitee.com/yanghuiyuan/saas-ui)
</div>

#### 项目简介
简云Saas平台 基于SpringBoot2.2.0, Mybatis, JWT, Redis, VUE+Element-UI 的前后端分离的Saas平台后台管理系统


 **产品分三个版本:
  1. 开源版本(包含了系统基础架构+在线表单设计). 此版本代码完全开源
  2. 个人学习版本( 在开源版本基础上增加 在线报表开发, 工作流设计, 代码生成模块 等其它系统优化模块).  
     获取代码，个人需支付 600RMB费用， 意向直接加作者QQ
  3. ERP版本(在个人学习版本基础上增加了ERP业务模块). 有意向合作的个人或企业欢迎咨询)** 

**开发文档**       [文档链接](http://yanghuiyuan.gitee.io/saas-ui/)

**体验地址**      [简云Saas平台](http://120.24.47.173/yhy/)

#### 项目源码

|     |   后端源码  |   前端源码  |
|---  |--- | --- |
|  码云   |  https://gitee.com/yanghuiyuan/saas-java   |  https://gitee.com/yanghuiyuan/saas-ui   |

####  系统功能

- 公司信息管理:      公司信息的维护及配置.
- 用户管理：         用户信息的维护及配置.
- 菜单管理：         菜单管理维护，动态配置菜单,按钮，字段权限.
- 我的组织机构管理:  可配置系统组织架构，树形表格展示
- 角色管理：        角色信息维护，可对数据权限(通过机构部门控制)，功能权限，用户与菜单进行分配。

- 字典管理：        配置一些常用固定的数据.
- 打印导出模板管理  用户可自定义配置报表输出格式(支持JXLS)

## - 自定义表单: 用于OA表单自定义申请, 自定义模块扩展功能.
## －表单业务申请: 表单业务申请，提交
## - 表格自定义列配置名称，宽度，列的顺序


--工作流定义: 集成了activity表单设计，部署，导入功能
- 工作流配置: 按模块+表单设计 工作流。

- 系统缓存：  使用RedisTemplate操作， 将redis可视化提供基本的操作.
- SQL监控：   使用druid 监控数据库
- 定时任务：  整合Quartz做定时任务.
- 代码生成：  待开发...


#### 项目结构
项目采用多模块开发方式(见下图)
 admin: 系统管理
 base: 基础数据
 business: 业务模块
 report: 报表模块
 api: 与第三方系统对接
 workflow: 工作流模块
 form: 表单定义模块
 job: 定时任务

<img src="https://images.gitee.com/uploads/images/2019/0904/172432_1ad97d33_1714834.png"/>
#### 系统预览
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101153_b6ab4ec5_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101213_4312698f_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101226_360193ec_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101241_621dbd99_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_d38bf99b_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_0c06dccd_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_75229eed_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_9edd937e_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_8ec89aac_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_e7cbff11_1714834.png"/></td>
    </tr>
    <tr>   
</table>


## 赞助

| 支付宝 | 微信 |
| ------------ | ------------ | 
| <img width="250" src="https://images.gitee.com/uploads/images/2019/0904/172432_05cdd716_1714834.png"/> |<img width="250" src="https://images.gitee.com/uploads/images/2019/0904/172432_39063f1a_1714834.png"/> |


#### 反馈交流
作者QQ: 417393356